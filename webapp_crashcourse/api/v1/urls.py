from rest_framework import routers

from .views import StudentViewSet

router = routers.DefaultRouter()
router.register('students', StudentViewSet)

app_name = 'api_v1'

urlpatterns = router.urls