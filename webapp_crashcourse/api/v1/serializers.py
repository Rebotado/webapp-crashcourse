from rest_framework import serializers
from student_score.models import (
    Student,
    Subject,
    Score
)

class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = '__all__'