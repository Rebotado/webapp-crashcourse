from rest_framework import viewsets
from .serializers import StudentSerializer
from student_score.models import Student


class StudentViewSet(viewsets.ModelViewSet):
    serializer_class = StudentSerializer
    queryset = Student.objects.all()

    @action(detail=False, methods=['POST'])
    def process_image(self, request, *args, **kwargs):
        if('image' in request.POST):
            image = request.POST['image']
            process_image(image)