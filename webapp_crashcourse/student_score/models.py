from django.db import models
from django.utils.translation import gettext as _

# Create your models here.
class Student(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    last_name = models.CharField(_('Last Name'), max_length=255)

class Subject(models.Model):
    name = models.CharField(_('Subject'), max_length=255)

class Score(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='scores')
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    score = models.IntegerField(_('Score'))


