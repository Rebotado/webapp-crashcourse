from django.shortcuts import render, reverse
from django.views.generic import (
    CreateView, 
    UpdateView, 
    DetailView, 
    DeleteView, 
    ListView, 
    TemplateView, 
)
from .models import (
    Student,
    Score,
    Subject
)

from .forms import (
    StudentForm,
    ScoreForm,
    SubjectForm,
)

class StudentCreateView(CreateView):
    template_name = 'student_scores/student/create.html'
    model = Student
    form_class = StudentForm

    def get_success_url(self):
        return reverse('student_score:student_details', kwargs={'pk':self.object.id})

class StudentDetailView(DetailView):
    template_name = 'student_scores/student/details.html'
    model = Student


class SubjectCreateView(CreateView):
    template_name = 'student_scores/subject/create.html'
    model = Subject
    form_class = SubjectForm

class ScoreCreateView(CreateView):
    template_name = 'student_scores/score/create.html'
    model = Score
    form_class = ScoreForm




class SomeView(TemplateView):
    template_name = 'student_scores/someview.html'