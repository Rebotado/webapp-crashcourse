from django.urls import path, include
from .views import (
    StudentCreateView, SubjectCreateView, ScoreCreateView,
    StudentDetailView,SomeView
)

app_name = 'student_score'

urlpatterns = [
    path('students/create', StudentCreateView.as_view(), name='student_create'),
    path('students/details/<int:pk>', StudentDetailView.as_view(), name='student_details'),
    path('subjects/create', SubjectCreateView.as_view(), name='subject_create'),
    path('scores/create', ScoreCreateView.as_view(), name='score_create'),
    path('someview', SomeView.as_view(), name='someview')
]