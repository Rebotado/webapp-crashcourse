from django.apps import AppConfig


class StudentScoreConfig(AppConfig):
    name = 'student_score'
